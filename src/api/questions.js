import { QUESTION_API_URL } from ".";

export async function apiFetchQuestions(difficulty, amount, category) {
  // const error = null;
  const url = `${QUESTION_API_URL}?difficulty=${difficulty}&amount=${amount}&category=${category}`;
  console.log(url);
  try {
    const response = await fetch(url);
    const questionsObj = await response.json();
    console.log("QuestionsObj:",questionsObj);
    return [questionsObj.response_code, questionsObj.results];
  } catch (error) {
    console.log(error);
    return [error, null];
  }
}
