import { USER_API_URL, USER_API_KEY } from ".";

// Checks if a user of this name exist and return the the first hit
// Other return false
export async function apiUserSubmit(username) {
  console.log(`${USER_API_URL}?username=${username}`);
  const error = null;
  try {
    const response = await fetch(`${USER_API_URL}?username=${username}`);
    const userArray = await response.json();
    let user = userArray[0];
    console.log("before", user);
    // userArray would be empty if no existing user was returned
    // If so, create new user
    if (!userArray.length) {
      console.log(`No such user: '${username}'`);
      user = await apiUserAdd(username);
      console.log("inside", user);
    }
    console.log("outside", user);
    return [error, user];
  } catch (error) {
    console.log(error);
  }
}

//  Create a new user with highScore 0
export async function apiUserAdd(username) {
  try {
    const config = {
      method: "POST",
      headers: {
        "X-API-Key": USER_API_KEY,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        highScore: "0",
      }),
    };
    const response = await fetch(USER_API_URL, config);
    const user = await response.json();
    if (!response.ok) {
      throw new Error("Could not create new user");
    } else {
      console.log("CREATED NEW USER", user);
      //   return [`New user created '${user.username}'`, user];
      return user;
    }
  } catch (error) {
    console.log("apiUserAdd error:", error);
  }
}

// export async function apiUserRegister(username) {
//   try {
//     const config = {
//       method: "POST",
//       headers: {
//         "content-type": "application/json",
//       },
//         body: JSON.stringify({
//           user: {
//             username,
//           },
//         }),
//     //   body: username,
//     };

//     const response = await fetch(`${USER_API_URL}?username=${username}`, config);
//     const json = await response.json();
//     const { username, highScore, id } = json[0];
//     return [null, username, id];
//   } catch (error) {
//     return [error.message, null];
//   }
// }
